FROM docker.k2.com/k2/tomcat:8.5

ENV SPRING_OUTPUT_ANSI_ENABLED=ALWAYS \
    JHIPSTER_SLEEP=0



# add directly the war
ADD cloud-test-1.0.0.war /usr/local/tomcat/webapps/ROOT.war

VOLUME /tmp
CMD echo "The application will start in ${JHIPSTER_SLEEP}s..." && \
    sleep ${JHIPSTER_SLEEP} && \
     /usr/local/tomcat/bin/startup.sh && tail -f /usr/local/tomcat/logs/catalina.out
